


# A Real time Camera Feed System for Criminal Detection!

## 1. Introduction
Undoubtedly, face recognition does have a very large variety of services and applications, beginning from human identification and surveillance and coming as far as e marketing and advertising for customers. The ability to efficiently recognize individuals through a combination of their facial characteristics (e g their eyes) is certainly an important phenomenon. So, the goal of recognizing peers through their faces is something already apparent on our everyday lives.

In our days, face recognition is more and more utilized in order to search and identify shoplifters, retail criminals, or people with a history of frauds or crimes. This means that their pictures can, after being processed, be matched against a large criminal database. As a result, this can prevent and reduce crime rates by identifying criminals from installed cameras.

## 2. The idea
All existing face recognition approaches can be used, extended and efficiently deployed for a wide variety of application in our everyday lives, ranging from outdoor face recognition for crime detection up to indoor object recognition and pattern matching. This is an extension of the existing facial identification approaches by proposing an online platform/website that can be used from the police forces.

This approach builds upon existing work and suggests not an algorithm, but a complete systemic approach through a crime detection platform to be used in police headquarter/precincts. The platform considers the Haar Cascade algorithm for the facial recognition detection and extends it by performing real-time recognition from the connected cameras in the system.

The platform supports two different types of users:
* Police employees in the headquarters/precincts
* Police Administrators, with a higher level of access and also responsible for database maintenance

The main features of the platform include:
* inserting, editing and deleting user and criminal information
* applying 6 different filters in the livestream cameras (no, gray, sepia, redish, blur, inverted)
* searching for criminals based on their picture through a livestream camera feed and identify them

As for the criminal identification, it is made possible through image pattern recognition between the provided criminal’s image and snapshots of identified faces from the livestream feed.

![enter image description here](https://i.ibb.co/zNCp9Rb/Picture14.png)

Image 1: The live feed facial recognition approach

The Live Feed section offers six different options for video filters, enabling the user to select the best filter, depending on the relevant situation of the physical surroundings (e.g. broad light, dark environment).

## 3. Tools needed

* Project Version : `Final`
* IDE Version : `InteliJ Pycharm 2019.3.3 Community Version`
* Programming Language : `Python`
* Web framework: `Flask`
* Python Version : `3.6.0`
* SQL Version : `MySQL RDBMS (XAMPP)`
* CSS Version : `Boostrap 4.0.0`


## 4. Building the database (MySQL)

The database of the platform consists of 3 different tables.

The `users` table: 

![enter image description here](https://i.ibb.co/KDBNbGw/1.png)

The `criminals` table: 

![enter image description here](https://i.ibb.co/NTWbwnh/2.png)

The `contact` table: 

![enter image description here](https://i.ibb.co/b3n867M/3.png)

## 5. The online platform

![enter image description here](https://i.ibb.co/NnJfRRW/Picture1.png)

Image 2: The welcoming page

![enter image description here](https://i.ibb.co/FxJymKd/Picture2.png)

Image 3: The Login page

![enter image description here](https://i.ibb.co/r0gg8Ps/Picture3.png)

Image 4: The Signup page

![enter image description here](https://i.ibb.co/s57HT6m/Picture4.png)

Image 5: The Home page

![enter image description here](https://i.ibb.co/gy9wbM8/Picture5.png)

Image 6: The Management page

![enter image description here](https://i.ibb.co/ng5pqhb/Picture6.png)

Image 7: The Insert users page (same idea for criminals)

![enter image description here](https://i.ibb.co/8MbKtfL/Picture7.png)

Image 8: The Management page for the criminals (same idea for users)

![enter image description here](https://i.ibb.co/fdFtn24/Picture8.png)

Image 9: Updating a record using modals

![enter image description here](https://i.ibb.co/vkG5h7T/Picture9.png)

Image 10: Report an issue to the system administrator

![enter image description here](https://i.ibb.co/3ppjhzn/Picture10.png)

Image 11: Selecting a criminal to search in live feed and the video filter

![enter image description here](https://i.ibb.co/59zCnrH/Picture11.png)
Image 12: The Last known location will be updated if criminal is recognized

![enter image description here](https://i.ibb.co/JnML3F3/Picture12.png)

Image 13: Criminal is recognized and his last known location is updated

![enter image description here](https://i.ibb.co/f9Ssz1F/Capture.png)

Image 14: The 6 different face recognition filters

![enter image description here](https://i.ibb.co/BLYgFmY/Picture13.png)

Image 15: Screenshots are saved inside a folder with the criminal's name, the datetime and the filter type are appended in the filename

# 6. Deploying the platform

The steps to deploy the website should be the following:

1. Creating the database
You may login in any MySQL database management panel and create a new database. You may use the `criminal_detection` name for your database. Make sure you select the UTF-8 encoding (collation `utf8_general_ci`). Both `app.py` and `camera.py` files include the following line for a connection to the MySQL database:
`mydb = MySQLdb.connect(db="criminal_detection", host="localhost", user="root", passwd="")`
You may update this according to your localhost setup and credentials.
2. Populating the database
Select your database (e.g. the `criminal_detection`) and import the `criminal_detection.sql` file, which includes the creation of the tables and the data population
3. Running the `app.py` file
This is the main file which includes the necessary app routings towards the HTML pages, alongside with the relevant data processing. The `camera.py` file is only called in the livestream page.
4. That's it! You have successfully deployed the website...